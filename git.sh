#!/bin/bash

function leer_commit (){

    until [[ $res == 'si' ]] || [[ $res == 'yes' ]] || [[ $res == 's' ]] || [[ $res == 'y' ]]
    do
        echo -e " Introduzca de nuevo commit: \n "
        read commit
        echo -e "¿Seguro? s/n y/n\n"
        read res
    done

}


echo -e "Empezando a subir apuntes.git\n"

git init
git add .

leer_commit

git commit -m "$commit"

echo -e "Se necesita verificación de USUARIO git\n"

git push

echo -e "TERMINADO"
